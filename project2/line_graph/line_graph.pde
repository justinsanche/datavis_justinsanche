Table table;
Bar[] bars;
int num = 0;

void setup(){
  size(600,600);
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    //loadData(selection);
    table = loadTable(selection.getAbsolutePath(), "header");
    load();
  }
}

void draw() {
 background(255);
 
 for(int i = 0; i<num; i++){
  bars[i].displayPoint();
  if(i<num-1)
    line(bars[i].x+bars[i].w/2,bars[i].y,bars[i+1].x+bars[i].w/2,bars[i+1].y);
 }
 
}

void load() {
  
  bars = new Bar[table.getRowCount()];
  
  for(int i=0; i<table.getRowCount(); i++) {
    
   TableRow row = table.getRow(i);
   
   float val1 = row.getFloat("YEAR");
   float val2 = row.getFloat("VALUE0");
   float val3 = row.getFloat("VALUE1");
   String string = row.getString("PARTY");
   
   float tempX = (600/table.getRowCount())*i;
   float tempY = 600-(val3*6);
   
   bars[i] = new Bar(tempX,tempY, 600.0/table.getRowCount(), 600-tempY,string);
   num++;
  }
  
}

class Bar {
  //x and y pos, width, height, and string label
  float x, y, w, h;
  String label;
  
  //contructor
  Bar(float temp1, float temp2, float temp3, float temp4, String temp5){
   x = temp1; 
   y = temp2;
   w = temp3;
   h = temp4;
   label = temp5;
  }
   
    //display a point on the line graph
   void displayPoint() {
    stroke(0);
    if(label.equals("REP"))
     fill(255,0,0);
    else if(label.equals("DEM"))
     fill(0,0,255);
    ellipse(x+w/2,y,10,10);
    }
    
  }