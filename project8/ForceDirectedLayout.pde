// most modification should occur in this file


class ForceDirectedLayout extends Frame {
  
  
  float RESTING_LENGTH = 10.0f;   // update this value
  float SPRING_SCALE   = 0.0075f; // update this value
  float REPULSE_SCALE  = 400.0f;  // update this value

  float TIME_STEP      = 0.5f;    // probably don't need to update this

  // Storage for the graph
  ArrayList<GraphVertex> verts;
  ArrayList<GraphEdge> edges;

  // Storage for the node selected using the mouse (you 
  // will need to set this variable and use it) 
  GraphVertex selected = null;
  

  ForceDirectedLayout( ArrayList<GraphVertex> _verts, ArrayList<GraphEdge> _edges ) {
    verts = _verts;
    edges = _edges;
  }

  void applyRepulsiveForce( GraphVertex v0, GraphVertex v1 ) {
    // TODO: PUT CODE IN HERE TO CALCULATE (AND APPLY) A REPULSIVE FORCE
    
    //println(v0.getPosition().x);
    
    float v0_xpos = v0.getPosition().x;
    float v0_ypos = v0.getPosition().y;
    float v1_xpos = v1.getPosition().x;
    float v1_ypos = v1.getPosition().y;
    
    //Force in x
    float xforce = (REPULSE_SCALE * v0.getMass() * v1.getMass())/(float)Math.pow(v1_xpos - v0_xpos, 2);
    
    //Force in y
    float yforce = (REPULSE_SCALE * v0.getMass() * v1.getMass())/(float)Math.pow(v1_ypos - v0_ypos, 2);
    
    v0.addForce( xforce, yforce );
    v1.addForce( xforce, yforce );
  }

  void applySpringForce( GraphEdge edge ) {
    // TODO: PUT CODE IN HERE TO CALCULATE (AND APPLY) A SPRING FORCE
    //println(edge.v0.getPosition().x);
    
    float v0_xpos = edge.v0.getPosition().x;
    float v0_ypos = edge.v0.getPosition().y;
    float v1_xpos = edge.v1.getPosition().x;
    float v1_ypos = edge.v1.getPosition().y;
    
    float xdistance = v1_xpos - v0_xpos;
    float ydistance = v1_ypos - v0_ypos;
    
    //force in x
    float xforce = SPRING_SCALE * max(0, xdistance - RESTING_LENGTH);
    
    //force in y
    float yforce = SPRING_SCALE * max(0, ydistance - RESTING_LENGTH);
    
    edge.v0.addForce( xforce, yforce );
    edge.v1.addForce( xforce, yforce );
  }

  void draw() {
    update(); // don't modify this line
    
    // TODO: ADD CODE TO DRAW THE GRAPH
    fill(255);
    stroke(0);
    
    for(int i = 0; i < verts.size(); i++)
    {
      if(verts.get(i).getPosition().x > 800 && verts.get(i).getPosition().y > 800)
      {
         verts.get(i).setPosition(0,0); 
      }
       ellipse(verts.get(i).getPosition().x, verts.get(i).getPosition().y, 5, 5);
    }
  }


  void mousePressed() { 
    // TODO: ADD SOME INTERACTION CODE

  }

  void mouseReleased() {    
    // TODO: ADD SOME INTERACTION CODE

  }



  // The following function applies forces to all of the nodes. 
  // This code does not need to be edited to complete this 
  // project (and I recommend against modifying it).
  void update() {
    for ( GraphVertex v : verts ) {
      v.clearForce();
    }

    for ( GraphVertex v0 : verts ) {
      for ( GraphVertex v1 : verts ) {
        if ( v0 != v1 ) applyRepulsiveForce( v0, v1 );
      }
    }

    for ( GraphEdge e : edges ) {
      applySpringForce( e );
    }

    for ( GraphVertex v : verts ) {
      v.updatePosition( TIME_STEP );
    }
  }
}