
JSONObject json;
JSONArray json_nodes, json_links;
Frame myFrame = null;

void setup() {
  size(800, 800);  
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } 
  else {
    println("User selected " + selection.getAbsolutePath());

    ArrayList<GraphVertex> verts = new ArrayList<GraphVertex>();
    ArrayList<GraphEdge>   edges = new ArrayList<GraphEdge>();


    // TODO: PUT CODE IN TO LOAD THE GRAPH    
    json = loadJSONObject(selection);
    json_nodes = json.getJSONArray("nodes");
    json_links = json.getJSONArray("links");
    
    //nodes
    for (int i = 0; i < json_nodes.size(); i++)
    {
       JSONObject node = json_nodes.getJSONObject(i);
       
       String id = node.getString("id");
       int group = node.getInt("group");
       float r1 = random(0,600);
       float r2 = random(0,600);
       GraphVertex vert = new GraphVertex(id, group, r1, r2);
       verts.add(vert);
    }
    
    //links
    for (int i = 0; i < json_links.size(); i++)
    {
       JSONObject link = json_links.getJSONObject(i);
       
       //GraphVertex node1 = null, node2 = null;
       int temp1 = 0, temp2 = 0;
       String source = link.getString("source");
       String target = link.getString("target");
       int weight = link.getInt("value");
       
       for (int j = 0; j < verts.size(); j++)
       {
           if (verts.get(j).getID() == source)
           {
               //node1 = verts.get(j);
               temp1 = j;
           }
           if (verts.get(j).getID() == target)
           {
               //node2 = verts.get(j);
               temp2 = j;
           }
       }
        
       GraphEdge edge = new GraphEdge(verts.get(temp1), verts.get(temp2), weight);
       edges.add(edge);
       
    }

    myFrame = new ForceDirectedLayout( verts, edges );
  }
}


void draw() {
  background( 255 );

  if ( myFrame != null ) {
    myFrame.setPosition( 0, 0, width, height );
    myFrame.draw();
  }
}

void mousePressed() {
  myFrame.mousePressed();
}

void mouseReleased() {
  myFrame.mouseReleased();
}

abstract class Frame {

  int u0, v0, w, h;
  int clickBuffer = 2;
  void setPosition( int u0, int v0, int w, int h ) {
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }

  abstract void draw();
  
  void mousePressed() { }
  void mouseReleased() { }

  boolean mouseInside() {
    return (u0-clickBuffer < mouseX) && (u0+w+clickBuffer)>mouseX && (v0-clickBuffer)< mouseY && (v0+h+clickBuffer)>mouseY;
  }
  
}