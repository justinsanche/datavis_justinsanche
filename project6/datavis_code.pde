
Frame myFrame = null;
Frame myFrame1 = null;
Frame myFrame2 = null;
Frame myFrame3 = null;
Frame myFrame4 = null;
int id1 = 0, id2 = 1;
Table table;

void setup(){
  size(800,600);  
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } else {
    println("User selected " + selection.getAbsolutePath());
    table = loadTable( selection.getAbsolutePath(), "header" );
    
    ArrayList<Integer> useColumns = new ArrayList<Integer>();
    for(int i = 0; i < table.getColumnCount(); i++){
      if( !Float.isNaN( table.getRow( 0 ).getFloat(i) ) ){
        println( i + " - type float" );
        useColumns.add(i);
      }
      else{
        println( i + " - type string" );
      }
    }
    
    myFrame = new PCP( table, useColumns );
    myFrame1 = new Splom( table, useColumns );
    myFrame2 = new Scatterplot( table, id1, id2 );
    myFrame3 = new dotFrame();
    myFrame4 = new dotFrame();
    
  }
}


void draw(){
  background( 255 );
  
  if( table == null ) 
    return;
  
  if( myFrame != null ){
       myFrame.setPosition( 0, 0, width/3, height/3 );
       myFrame.draw();
  }
       
  if( myFrame1 != null ){
       myFrame1.setPosition( width/3, height/3, width/3, height/3 );
       myFrame1.draw();
  }
  
  if( myFrame2 != null ){
       myFrame2.setPosition( 2*(width/3), 0, width/3, height/3 );
       myFrame2.draw();
  }
  
  if( myFrame3 != null ){
     myFrame3.setPosition( 0, 2*(height/3), width/3, height/3 );
     myFrame3.draw();
  }
  
  if( myFrame4 != null ){
     myFrame4.setPosition(2*(width/3), 2*(height/3), width/3, height/3 );
     myFrame4.draw();
  }
}

void mousePressed(){
  myFrame.mousePressed();
}

void mouseReleased(){
  myFrame.mouseReleased();
}

abstract class Frame {
  
  int u0,v0,w,h;
     int clickBuffer = 2;
  void setPosition( int u0, int v0, int w, int h ){
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }
  
  abstract void draw();
  void mousePressed(){ }
  void mouseReleased(){ }
  
   boolean mouseInside(){
      return (u0-clickBuffer < mouseX) && (u0+w+clickBuffer)>mouseX && (v0-clickBuffer)< mouseY && (v0+h+clickBuffer)>mouseY; 
   }
  
  
}