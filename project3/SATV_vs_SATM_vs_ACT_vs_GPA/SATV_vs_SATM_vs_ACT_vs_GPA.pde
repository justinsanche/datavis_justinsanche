Table table;
PFont f;

void setup(){
  size(800,600);
  f = createFont("Arial",16,true);
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    //loadData(selection);
    table = loadTable(selection.getAbsolutePath(), "header");
    //load();
  }
}

void draw(){
  if(table == null) return;
  
  //purple-ish background and black lines
  background(255,204,229);
  stroke(0);
  textFont(f,16);                 
  fill(0);
  
  //set up outlines of graphs
  line(1, height * 0.1, width-2, height * 0.1);
  line(1, height * 0.1, 1, height * 0.4);
  line(1, height * 0.4, width-2, height * 0.4);
  line(width/3, height * 0.1, width/3, height * 0.7);
  line(width/3, height * 0.7, width-2, height * 0.7);
  line((2*width)/3, height * 0.1, (2*width)/3, height-2);
  line((2*width)/3, height-2, width-2, height-2);
  line(width-2, height * 0.1, width-2, height-2);
  
  //legend/scale x, y
  line(width * 0.05, height * 0.7, width * 0.05, height * 0.9);
  line(width * 0.05, height * 0.7, width * 0.3, height * 0.7);
  
  textAlign(CENTER);
  
  //title
  text("SATV vs SATM vs ACT vs GPA", width/2, height * 0.03);
  
  //labels for scale
  textFont(f, 10);
  text("800", width * 0.03, height * 0.9);
  text("0", width * 0.03, height * 0.67);
  text("800", width * 0.3, height * 0.67);
  text("Scale/Legend", width * 0.165, height * 0.65);
  
  //iterate through each row of the table
  for(int i = 0; i<table.getRowCount(); i++)
  {
   TableRow row = table.getRow(i);
   float SATM = row.getFloat("SATM");
   float SATV = row.getFloat("SATV");
   float ACT = row.getFloat("ACT");
   float GPA = row.getFloat("GPA");
   
   float x = 0, y = 0;
   
   //create point SATM = x, SATV = y
   x = SATM;
   y = SATV;
   x = map(x, 0, 800, 1, width/3-5);
   y = map(y, 0, 800, height * 0.1, height * 0.39); 
   ellipse(x, y, 3, 3);
   
   //create point SATM = x, ACT = y
   x = SATM;
   y = ACT;
   x = map(x, 0, 800, width/3, 2*width/3-5);
   y = map(y, 0, 36, height * 0.1, height * 0.39);
   ellipse(x, y, 3, 3);
   
   //create point SATM = x, GPA = y
   x = SATM;
   y = GPA;
   x = map(x, 0, 800, 2*width/3, width-5);
   y = map(y, 0, 4, height * 0.1, height * 0.39);
   ellipse(x, y, 3, 3);
   
   //create point SATV = x, ACT = y
   x = SATV;
   y = ACT;
   x = map(x, 0, 800, width/3, 2*width/3-5);
   y = map(y, 0, 36, height * 0.4, height * 0.69);
   ellipse(x, y, 3, 3);
   
   //create point SATV = x, GPA = y
   x = SATV;
   y = GPA;
   x = map(x, 0, 800, 2*width/3, width-5);
   y = map(y, 0, 4, height * 0.4, height * 0.69);
   ellipse(x, y, 3, 3);
   
   //create point ACT = x, GPA = y
   x = ACT;
   y = GPA;
   x = map(x, 0, 36, 2*width/3, width-5);
   y = map(y, 0, 4, height * 0.7, height - 5);
   ellipse(x, y, 3, 3);
   
  }
}