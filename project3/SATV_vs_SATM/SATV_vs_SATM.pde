Table table;
PFont f;

void setup(){
  size(800,600);
  f = createFont("Arial",16,true);
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    //loadData(selection);
    table = loadTable(selection.getAbsolutePath(), "header");
    //load();
  }
}

void draw(){
  if(table == null) return;
  
  //light blue background and black lines
  background(204, 229, 255);
  stroke(0);
  textFont(f,16);                 
  fill(0);
  
  //x and y axis
  float y_xTop = width * 0.1;
  float y_yTop = height * 0.1;
  float y_xBot = width * 0.1;
  float y_yBot = height * 0.9;
  
  float x_xLeft = width * 0.9;
  float x_yLeft = height * 0.1;
  float x_xRight = width * 0.1;
  float x_yRight = height * 0.1;
  
  //graph outline
  line(y_xTop, y_yTop, y_xBot, y_yBot);
  line(x_xLeft, x_yLeft, x_xRight, x_yRight);
  
  textAlign(CENTER);
  
  //title, x and y axis labels
  text("SATM vs SATV Scores", width/2, height * 0.03);
  //x
  text("SATM", width/2, height * 0.08);
  rotate(3*PI/2);
  //y
  text("SATV", width * -0.35, height * 0.04);
  rotate(PI/2);
  //scale/labels
  text("800", width * 0.07, height * 0.93);
  text("0", width * 0.07, height * 0.1);
  text("800", width * 0.9, height * 0.07);
  
  //iterate through the rows
  for(int i = 0; i<table.getRowCount(); i++)
  {
   TableRow row = table.getRow(i);
   float SATM = row.getFloat("SATM");
   float SATV = row.getFloat("SATV");
   
   //create point SATM = x, SATV = y
   ellipse(SATM * 0.8 + 80, SATV * 0.6 + 60, 3, 3);
   
  }
}