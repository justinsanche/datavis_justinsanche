Table table;
PFont f;
float buttonCounter = 0;

void setup(){
  size(800,600);
  f = createFont("Arial",16,true);
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } else {
    println("User selected " + selection.getAbsolutePath());
    table = loadTable(selection.getAbsolutePath(), "header");
  }
}

void mousePressed(){
  if((mouseX <= 295) && (mouseX >= 145) && (mouseY <= 45) && (mouseY >= 15))
  {
    buttonCounter++;
    if(buttonCounter > 5)
      buttonCounter = 0;
  }
}

void draw(){
  if(table == null) return;
  
  //light blue background and black lines
  background(204, 229, 255);
  
  //button
  stroke(0);
  fill(255);
  rect(145, 15, 150, 30);
  fill(0);
  text("Change Data", 220, 40);
  
  stroke(0);
  textFont(f,16);                 
  
  //x and y axis
  float y_xTop = width * 0.1;
  float y_yTop = height * 0.1;
  float y_xBot = width * 0.1;
  float y_yBot = height * 0.9;
  
  float x_xLeft = width * 0.9;
  float x_yLeft = height * 0.9;
  float x_xRight = width * 0.1;
  float x_yRight = height * 0.9;
  
  //graph outline
  line(y_xTop, y_yTop, y_xBot, y_yBot);
  line(x_xLeft, x_yLeft, x_xRight, x_yRight);
  
  textAlign(CENTER);
  
  //0 is column 0 vs column 1
  if(buttonCounter == 0)
  {
  //title, x and y axis labels
  text(table.getColumnTitle(0) + " vs " + table.getColumnTitle(1), width/2, height * 0.03);
  //x
  text(table.getColumnTitle(0), width/2, height * 0.96);
  rotate(3*PI/2);
  //y
  text(table.getColumnTitle(1), width * -0.35, height * 0.04);
  rotate(PI/2);
  //scale/labels x then y
  float maxx = max(table.getFloatColumn(0));
  float maxy = max(table.getFloatColumn(1));
  text(maxx, width * 0.9, height * 0.93);
  text("0", width * 0.11, height * 0.93);
  text("0", width * 0.09, height * 0.89);
  text(maxy, width * 0.1, height * 0.09);
  
  //iterate through the rows
  for(int i = 0; i<table.getRowCount(); i++)
  {
   TableRow row = table.getRow(i);
   float row0 = row.getFloat(table.getColumnTitle(0));
   float row1 = row.getFloat(table.getColumnTitle(1));
   
   
   //create point
   row0 = map(row0, 0, maxx, x_xRight, x_xLeft);
   row1 = map(row1, 0, maxy, y_yBot, y_yTop);
     ellipse(row0 , row1 , 3, 3);
     
   //hover over data
   if((mouseX >= row0-2) && (mouseX <= row0+2) && (mouseY >= row1-2) && (mouseY <= row1+2))
   {
     text(table.getColumnTitle(0) + ": " + row.getFloat(table.getColumnTitle(0)), width*0.2, height*0.95);
     text(table.getColumnTitle(1) + ": " + row.getFloat(table.getColumnTitle(1)), width*0.35, height*0.95);
   }
   
  }
  }//end of case 0
  
  //1 is column 0 vs column 2
  if(buttonCounter == 1)
  {
  //title, x and y axis labels
  text(table.getColumnTitle(0) + " vs " + table.getColumnTitle(2), width/2, height * 0.03);
  //x
  text(table.getColumnTitle(0), width/2, height * 0.96);
  rotate(3*PI/2);
  //y
  text(table.getColumnTitle(2), width * -0.35, height * 0.04);
  rotate(PI/2);
  //scale/labels x then y
  float maxx = max(table.getFloatColumn(0));
  float maxy = max(table.getFloatColumn(2));
  text(maxx, width * 0.9, height * 0.93);
  text("0", width * 0.11, height * 0.93);
  text("0", width * 0.09, height * 0.89);
  text(maxy, width * 0.1, height * 0.09);
  
  //iterate through the rows
  for(int i = 0; i<table.getRowCount(); i++)
  {
   TableRow row = table.getRow(i);
   float row0 = row.getFloat(table.getColumnTitle(0));
   float row2 = row.getFloat(table.getColumnTitle(2));
   
   //create point
   row0 = map(row0, 0, maxx, x_xRight, x_xLeft);
   row2 = map(row2, 0, maxy, y_yBot, y_yTop);
   ellipse(row0 , row2 , 3, 3);
   
   //hover over data
   if((mouseX >= row0-2) && (mouseX <= row0+2) && (mouseY >= row2-2) && (mouseY <= row2+2))
   {
     text(table.getColumnTitle(0) + ": " + row.getFloat(table.getColumnTitle(0)), width*0.2, height*0.95);
     text(table.getColumnTitle(2) + ": " + row.getFloat(table.getColumnTitle(2)), width*0.35, height*0.95);
   }
  }
  }//end of case 1
  
  //2 is column 0 vs column 3
  if(buttonCounter == 2)
  {
  //title, x and y axis labels
  text(table.getColumnTitle(0) + " vs " + table.getColumnTitle(3), width/2, height * 0.03);
  //x
  text(table.getColumnTitle(0), width/2, height * 0.96);
  rotate(3*PI/2);
  //y
  text(table.getColumnTitle(3), width * -0.35, height * 0.04);
  rotate(PI/2);
  //scale/labels x then y
  float maxx = max(table.getFloatColumn(0));
  float maxy = max(table.getFloatColumn(3));
  text(maxx, width * 0.9, height * 0.93);
  text("0", width * 0.11, height * 0.93);
  text("0", width * 0.09, height * 0.89);
  text(maxy, width * 0.1, height * 0.09);
  
  //iterate through the rows
  for(int i = 0; i<table.getRowCount(); i++)
  {
   TableRow row = table.getRow(i);
   float row0 = row.getFloat(table.getColumnTitle(0));
   float row3 = row.getFloat(table.getColumnTitle(3));
   
   //create point
   row0 = map(row0, 0, maxx, x_xRight, x_xLeft);
   row3 = map(row3, 0, maxy, y_yBot, y_yTop);
   ellipse(row0 , row3 , 3, 3);
   
   //hover over data
   if((mouseX >= row0-2) && (mouseX <= row0+2) && (mouseY >= row3-2) && (mouseY <= row3+2))
   {
     text(table.getColumnTitle(0) + ": " + row.getFloat(table.getColumnTitle(0)), width*0.2, height*0.95);
     text(table.getColumnTitle(3) + ": " + row.getFloat(table.getColumnTitle(3)), width*0.35, height*0.95);
   }
  }
  }//end of case 2
  
  //3 is column 1 vs column 2
  if(buttonCounter == 3)
  {
  //title, x and y axis labels
  text(table.getColumnTitle(1) + " vs " + table.getColumnTitle(2), width/2, height * 0.03);
  //x
  text(table.getColumnTitle(1), width/2, height * 0.96);
  rotate(3*PI/2);
  //y
  text(table.getColumnTitle(2), width * -0.35, height * 0.04);
  rotate(PI/2);
  //scale/labels x then y
  float maxx = max(table.getFloatColumn(1));
  float maxy = max(table.getFloatColumn(2));
  text(maxx, width * 0.9, height * 0.93);
  text("0", width * 0.11, height * 0.93);
  text("0", width * 0.09, height * 0.89);
  text(maxy, width * 0.1, height * 0.09);
  
  //iterate through the rows
  for(int i = 0; i<table.getRowCount(); i++)
  {
   TableRow row = table.getRow(i);
   float row1 = row.getFloat(table.getColumnTitle(1));
   float row2 = row.getFloat(table.getColumnTitle(2));
   
   //create point
   row1 = map(row1, 0, maxx, x_xRight, x_xLeft);
   row2 = map(row2, 0, maxy, y_yBot, y_yTop);
   ellipse(row1 , row2 , 3, 3);
   
   //hover over data
   if((mouseX >= row1-2) && (mouseX <= row1+2) && (mouseY >= row2-2) && (mouseY <= row2+2))
   {
     text(table.getColumnTitle(1) + ": " + row.getFloat(table.getColumnTitle(1)), width*0.2, height*0.95);
     text(table.getColumnTitle(2) + ": " + row.getFloat(table.getColumnTitle(2)), width*0.35, height*0.95);
   }
  }
  }//end of case 3
  
  //4 is column 1 vs column 3
  if(buttonCounter == 4)
  {
  //title, x and y axis labels
  text(table.getColumnTitle(1) + " vs " + table.getColumnTitle(3), width/2, height * 0.03);
  //x
  text(table.getColumnTitle(1), width/2, height * 0.96);
  rotate(3*PI/2);
  //y
  text(table.getColumnTitle(3), width * -0.35, height * 0.04);
  rotate(PI/2);
  //scale/labels x then y
  float maxx = max(table.getFloatColumn(1));
  float maxy = max(table.getFloatColumn(3));
  text(maxx, width * 0.9, height * 0.93);
  text("0", width * 0.11, height * 0.93);
  text("0", width * 0.09, height * 0.89);
  text(maxy, width * 0.1, height * 0.09);
  
  //iterate through the rows
  for(int i = 0; i<table.getRowCount(); i++)
  {
   TableRow row = table.getRow(i);
   float row1 = row.getFloat(table.getColumnTitle(1));
   float row3 = row.getFloat(table.getColumnTitle(3));
   
   //create point
   row1 = map(row1, 0, maxx, x_xRight, x_xLeft);
   row3 = map(row3, 0, maxy, y_yBot, y_yTop);
   ellipse(row1 , row3 , 3, 3);
   
   //hover over data
   if((mouseX >= row1-2) && (mouseX <= row1+2) && (mouseY >= row3-2) && (mouseY <= row3+2))
   {
     text(table.getColumnTitle(1) + ": " + row.getFloat(table.getColumnTitle(1)), width*0.2, height*0.95);
     text(table.getColumnTitle(3) + ": " + row.getFloat(table.getColumnTitle(3)), width*0.35, height*0.95);
   }
  }
  }//end of case 4
  
  //5 is column 2 vs column 3
  if(buttonCounter == 5)
  {
  //title, x and y axis labels
  text(table.getColumnTitle(2) + " vs " + table.getColumnTitle(3), width/2, height * 0.03);
  //x
  text(table.getColumnTitle(2), width/2, height * 0.96);
  rotate(3*PI/2);
  //y
  text(table.getColumnTitle(3), width * -0.35, height * 0.04);
  rotate(PI/2);
  //scale/labels x then y
  float maxx = max(table.getFloatColumn(2));
  float maxy = max(table.getFloatColumn(3));
  text(maxx, width * 0.9, height * 0.93);
  text("0", width * 0.11, height * 0.93);
  text("0", width * 0.09, height * 0.89);
  text(maxy, width * 0.1, height * 0.09);
  
  //iterate through the rows
  for(int i = 0; i<table.getRowCount(); i++)
  {
   TableRow row = table.getRow(i);
   float row2 = row.getFloat(table.getColumnTitle(2));
   float row3 = row.getFloat(table.getColumnTitle(3));
   
   //create point
   row2 = map(row2, 0, maxx, x_xRight, x_xLeft);
   row3 = map(row3, 0, maxy, y_yBot, y_yTop);
   ellipse(row2 , row3 , 3, 3);
   
   //hover over data
   if((mouseX >= row2-2) && (mouseX <= row2+2) && (mouseY >= row3-2) && (mouseY <= row3+2))
   {
     text(table.getColumnTitle(2) + ": " + row.getFloat(table.getColumnTitle(2)), width*0.2, height*0.95);
     text(table.getColumnTitle(3) + ": " + row.getFloat(table.getColumnTitle(3)), width*0.35, height*0.95);
   }
  }
  }//end of case 5
}