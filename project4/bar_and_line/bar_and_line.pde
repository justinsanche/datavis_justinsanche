Table table;
PFont f;
float buttonCounter = 0;
//Bar[] bars;
int num = 0;

void setup(){
  size(800,600);
  f = createFont("Arial",16,true);
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } else {
    println("User selected " + selection.getAbsolutePath());
    //loadData(selection);
    table = loadTable(selection.getAbsolutePath(), "header");
    //load();
  }
}

void mousePressed(){
  if((mouseX <= 295) && (mouseX >= 145) && (mouseY <= 45) && (mouseY >= 15))
  {
    buttonCounter++;
    if(buttonCounter > 5)
      buttonCounter = 0;
  }
}

void draw() {
 if(table == null) return;
 background(255);
 num = 0;
 //button
  stroke(0);
  fill(255);
  rect(145, 15, 150, 30);
  fill(0);
  text("Change Data", 220, 40);
  
  stroke(0);
  textFont(f,16);
  
  //x and y axis
  float y_xTop = width * 0.1;
  float y_yTop = height * 0.1;
  float y_xBot = width * 0.1;
  float y_yBot = height * 0.9;
  
  float x_xLeft = width * 0.9;
  float x_yLeft = height * 0.9;
  float x_xRight = width * 0.1;
  float x_yRight = height * 0.9;
  
  //graph outline
  line(y_xTop, y_yTop, y_xBot, y_yBot);
  line(x_xLeft, x_yLeft, x_xRight, x_yRight);
  
  textAlign(CENTER);
  
  //0 is column 0 vs column 1
  if(buttonCounter == 0)
  {
   //title, x and y axis labels
  text(table.getColumnTitle(0) + " vs " + table.getColumnTitle(1), width/2, height * 0.03);
  //x
  text(table.getColumnTitle(0), width/2, height * 0.96);
  rotate(3*PI/2);
  //y
  text(table.getColumnTitle(1), width * -0.35, height * 0.04);
  rotate(PI/2);
  //scale/labels x then y
  float maxx = max(table.getFloatColumn(0));
  float maxy = max(table.getFloatColumn(1));
  text(maxx, width * 0.9, height * 0.93);
  text("0", width * 0.11, height * 0.93);
  text("0", width * 0.09, height * 0.89);
  text(maxy, width * 0.1, height * 0.09);
    
    //iterate through the rows
  for(int i = 0; i<table.getRowCount(); i++)
  {
   TableRow row = table.getRow(i);
   float row0 = row.getFloat(table.getColumnTitle(0));
   float row1 = row.getFloat(table.getColumnTitle(1));
   
   
   //create bar
   row0 = map(row0, 0, maxx, x_xRight, x_xLeft);
   row1 = map(row1, 0, maxy, y_yBot, y_yTop);
   rect(((width*0.8)/table.getRowCount())*num+width*0.1 ,row1 ,(width*0.8)/table.getRowCount(),height*0.9-row1);
   num++;
   
   //hover over data
   if((mouseX >= row0-2) && (mouseX <= row0+2) && (mouseY >= height*0.9) && (mouseY <= row1))
   {
     text(table.getColumnTitle(0) + ": " + row.getFloat(table.getColumnTitle(0)), width*0.2, height*0.95);
     text(table.getColumnTitle(1) + ": " + row.getFloat(table.getColumnTitle(1)), width*0.35, height*0.95);
   }
   
  }
  }//end of case 0
    
  
 /*for(int i = 0; i<num; i++){
  bars[i].displayPoint();
  bars[i].displayBar();
  if(i<num-1)
    line(bars[i].x+bars[i].w/2,bars[i].y,bars[i+1].x+bars[i].w/2,bars[i+1].y);
 }*/
 
}

/*void load() {
  
  bars = new Bar[table.getRowCount()];
  
  for(int i=0; i<table.getRowCount(); i++) {
    
   TableRow row = table.getRow(i);
   
   float val1 = row.getFloat("YEAR");
   float val2 = row.getFloat("VALUE0");
   float val3 = row.getFloat("VALUE1");
   String string = row.getString("PARTY");
   
   float tempX = (600/table.getRowCount())*i;
   float tempY = 600-(val3*6);
   
   bars[i] = new Bar(tempX,tempY, 600.0/table.getRowCount(), 600-tempY,string);
   num++;
  }
  
}

class Bar {
  //x and y pos, width, height, and string label
  float x, y, w, h;
  String label;
  
  //contructor
  Bar(float temp1, float temp2, float temp3, float temp4, String temp5){
   x = temp1; 
   y = temp2;
   w = temp3;
   h = temp4;
   label = temp5;
  }
  
  //print a bar
  void display() {
    
   stroke(0);
   if(label.equals("REP"))
     fill(255,0,0);
   else if(label.equals("DEM"))
     fill(0,0,255);
   rect(x,y,w,h);
   }
    
   //print a point on the line graph
   void displayPoint() {
    stroke(0);
    if(label.equals("REP"))
     fill(255,0,0);
    else if(label.equals("DEM"))
     fill(0,0,255);
    ellipse(x+w/2,y,10,10);
    }
    
    //print a bar
  void displayBar() {
   stroke(0);
   if(label.equals("REP"))
     fill(255,0,0);
   else if(label.equals("DEM"))
     fill(0,0,255);
   rect(x,y,w,h);
   }
    
  }*/