Table table;
PFont f;
int counter = 0;

void setup(){
  size(800,600);
  f = createFont("Arial",16,true);
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } else {
    println("User selected " + selection.getAbsolutePath());
    table = loadTable(selection.getAbsolutePath(), "header");
  }
}

void mousePressed(){
  if((mouseX >= 1) && (mouseX < width/3))
  {
    if((mouseY >= height*0.1) && (mouseY < height*0.4))
    {
      counter = 1; //top left
    }
  }
  if((mouseX >= width/3) && (mouseX < (2*width)/3))
  {
    if((mouseY >= height*0.1) && (mouseY < height*0.4))
    {
      counter = 2; //top middle 
    }
    if((mouseY >= height*0.4) && (mouseY <= height*0.7))
    {
      counter = 4; //midle middle 
    }
  }
  if((mouseX >= (2*width)/3) && (mouseX < width))
  {
    if((mouseY >= height*0.1) && (mouseY < height*0.4))
    {
      counter = 3; //top right 
    }
    if((mouseY >= height*0.4) && (mouseY < height*0.7))
    {
      counter = 5; //midle right 
    }
    if((mouseY >= height*0.7) && (mouseY < height))
    {
      counter = 6; //bottom right
    }
  }
}

void draw(){
  if(table == null) return;
  
  background(255);
  stroke(0);
  textFont(f,16);                 
  fill(0);
  
  //set up outlines of graphs
  line(1, height * 0.1, width-2, height * 0.1);
  line(1, height * 0.1, 1, height * 0.4);
  line(1, height * 0.4, width-2, height * 0.4);
  line(width/3, height * 0.1, width/3, height * 0.7);
  line(width/3, height * 0.7, width-2, height * 0.7);
  line((2*width)/3, height * 0.1, (2*width)/3, height-2);
  line((2*width)/3, height-2, width-2, height-2);
  line(width-2, height * 0.1, width-2, height-2);
  
  textAlign(CENTER);
  
  //title
  text(table.getColumnTitle(0) + " vs " + table.getColumnTitle(1) + " vs " + table.getColumnTitle(2) + " vs " + table.getColumnTitle(3), width/2, height * 0.03);
  
  //labels for scale
  textFont(f, 12);
  
  //iterate through each row of the table
  for(int i = 0; i<table.getRowCount(); i++)
  {
   TableRow row = table.getRow(i);
   float col0 = row.getFloat(table.getColumnTitle(0));
   float col1 = row.getFloat(table.getColumnTitle(1));
   float col2 = row.getFloat(table.getColumnTitle(2));
   float col3 = row.getFloat(table.getColumnTitle(3));
   float c0Max = max(table.getFloatColumn(0));
   float c1Max = max(table.getFloatColumn(1));
   float c2Max = max(table.getFloatColumn(2));
   float c3Max = max(table.getFloatColumn(3));
   
   float x = 0, y = 0;
   
   //create point SATM = x, SATV = y
   x = col0;
   y = col1;
   x = map(x, 0, c0Max, 1, width/3-5);
   y = map(y, 0, c1Max, height * 0.1, height * 0.39); 
   ellipse(x, y, 3, 3);
   
   //create point SATM = x, ACT = y
   x = col0;
   y = col2;
   x = map(x, 0, c0Max, width/3, 2*width/3-5);
   y = map(y, 0, c2Max, height * 0.1, height * 0.39);
   ellipse(x, y, 3, 3);
   
   //create point SATM = x, GPA = y
   x = col0;
   y = col3;
   x = map(x, 0, c0Max, 2*width/3, width-5);
   y = map(y, 0, c3Max, height * 0.1, height * 0.39);
   ellipse(x, y, 3, 3);
   
   //create point SATV = x, ACT = y
   x = col1;
   y = col2;
   x = map(x, 0, c1Max, width/3, 2*width/3-5);
   y = map(y, 0, c2Max, height * 0.4, height * 0.69);
   ellipse(x, y, 3, 3);
   
   //create point SATV = x, GPA = y
   x = col1;
   y = col3;
   x = map(x, 0, c1Max, 2*width/3, width-5);
   y = map(y, 0, c3Max, height * 0.4, height * 0.69);
   ellipse(x, y, 3, 3);
   
   //create point ACT = x, GPA = y
   x = col2;
   y = col3;
   x = map(x, 0, c2Max, 2*width/3, width-5);
   y = map(y, 0, c3Max, height * 0.7, height - 5);
   ellipse(x, y, 3, 3);
   
   textAlign(LEFT);
   
   //bigger graph in bottom
   if(counter == 1)
   {
     
    line(1, height*0.6, 1, height-1); //y
    line(1, height-1, width*0.4, height-1); //x
    text(int(c0Max), 5, height*0.61); 
    text(int(c1Max), width*0.41, height-1);
    text("0", 5, height-5);
    text(table.getColumnTitle(0) + " vs " + table.getColumnTitle(1), width*0.2, height*0.55);
    
   x = col0;
   y = col1;
   x = map(x, 0, c0Max, 2, width*0.4);
   y = map(y, 0, c1Max, height-1, height*0.6); 
   ellipse(x, y, 3, 3);
   }
   if(counter == 2)
   {
     
    line(1, height*0.6, 1, height-1); //y
    line(1, height-1, width*0.4, height-1); //x
    text(int(c0Max), 5, height*0.61); 
    text(int(c2Max), width*0.41, height-1);
    text("0", 5, height-5);
    text(table.getColumnTitle(0) + " vs " + table.getColumnTitle(2), width*0.2, height*0.55);
  
   x = col0;
   y = col2;
   x = map(x, 0, c0Max, 2, width*0.4);
   y = map(y, 0, c2Max, height-1, height*0.6); 
   ellipse(x, y, 3, 3);
   }
   if(counter == 3)
   {
     
    line(1, height*0.6, 1, height-1); //y
    line(1, height-1, width*0.4, height-1); //x
    text(int(c0Max), 5, height*0.61); 
    text(int(c3Max), width*0.41, height-1);
    text("0", 5, height-5);
    text(table.getColumnTitle(0) + " vs " + table.getColumnTitle(3), width*0.2, height*0.55);
    
   x = col0;
   y = col3;
   x = map(x, 0, c0Max, 2, width*0.4);
   y = map(y, 0, c3Max, height-1, height*0.6); 
   ellipse(x, y, 3, 3);
   }
   if(counter == 4)
   {
     
    line(1, height*0.6, 1, height-1); //y
    line(1, height-1, width*0.4, height-1); //x
    text(int(c1Max), 5, height*0.61); 
    text(int(c2Max), width*0.41, height-1);
    text("0", 5, height-5);
    text(table.getColumnTitle(1) + " vs " + table.getColumnTitle(2), width*0.2, height*0.55);
   
   x = col1;
   y = col2;
   x = map(x, 0, c1Max, 2, width*0.4);
   y = map(y, 0, c2Max, height-1, height*0.6); 
   ellipse(x, y, 3, 3);
   }
   if(counter == 5)
   {
     
    line(1, height*0.6, 1, height-1); //y
    line(1, height-1, width*0.4, height-1); //x
    text(int(c1Max), 5, height*0.61); 
    text(int(c3Max), width*0.41, height-1);
    text("0", 5, height-5);
    text(table.getColumnTitle(1) + " vs " + table.getColumnTitle(3), width*0.2, height*0.55);
   
   x = col1;
   y = col3;
   x = map(x, 0, c1Max, 2, width*0.4);
   y = map(y, 0, c3Max, height-1, height*0.6); 
   ellipse(x, y, 3, 3);
   }
   if(counter == 6)
   {
     
    line(1, height*0.6, 1, height-1); //y
    line(1, height-1, width*0.4, height-1); //x
    text(int(c2Max), 5, height*0.61); 
    text(int(c3Max), width*0.41, height-1);
    text("0", 5, height-5);
    text(table.getColumnTitle(2) + " vs " + table.getColumnTitle(3), width*0.2, height*0.55);

   x = col2;
   y = col3;
   x = map(x, 0, c2Max, 2, width*0.4);
   y = map(y, 0, c3Max, height-1, height*0.6); 
   ellipse(x, y, 3, 3);
   }
   
  }
}